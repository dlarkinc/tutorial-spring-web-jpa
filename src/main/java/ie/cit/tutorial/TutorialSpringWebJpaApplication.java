package ie.cit.tutorial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TutorialSpringWebJpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(TutorialSpringWebJpaApplication.class, args);
    }
}
